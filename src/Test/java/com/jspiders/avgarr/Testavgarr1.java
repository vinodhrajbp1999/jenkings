package Test.java.com.jspiders.avgarr;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import main.java.com.jspiders.avgarr.avgarr1;

class Testavgarr1 {

	@Test
	void testAvgarr() {
		assertEquals(10, avgarr1.sumAvg(new int[] {10,10,10}));
		assertEquals(20, avgarr1.sumAvg(new int[] {20,20,20}));
		assertEquals(20, avgarr1.sumAvg(new int[] {20,20,20}));
		
	}

}
